<?php 

class Home extends Controller
{
    public function index()
    {   
        $data['judul'] = 'Home';
        $data['nama'] = $this->model('User_model')->getUser();
        $this->view('template/header', $data);
        $this->view('home/index', $data);
        $this->view('template/footer', $data);

    }

    public function login()
    {
        if (isset($_POST['username'])) {
            $user = $this->model('User_model')->getUserByUsername($_POST['username']);

            // Cek user ada atau tidak
            if (!$user) {
                header('Location: ' . BASE_URL . '/home/login');
                exit;
            }

            // Cek password benar / salah
            if (password_verify($_POST['password'], $user['password'])) {
                header('Location: ' . BASE_URL . '/home/index');
                exit;    
            }
        }

    }

    public function about($company = 'SMKN1')
    {
        $data["judul"] = "About";
        $data["company"] = $company;
        $data['user'] = $this->model('User_model')->getAllUser();
        $this->view('template/header', $data);
        $this->view('home/index', $data);
        $this->view('template/footer', $data);
    }

    public function show($id){
        $data['users'] = $this->model('User_model')->getAllUser();
        $data['user'] = $this->model('User_model')->getUserById($id);
        $this->view('templates/header', $data);
        $this->view('home/index', $data);
        $this->view('templates/footer', $data);
    }

    public function create(){
        if($this->model('User_model')->createUser($_POST) > 0){
            header('Location: ' . BASE_URL . '/home/about' );
            exit;
        } else {
            echo("error");
        }
    }

    public function profile(){
        $data['title'] = 'Profile';
        
        $this->view('templates/header', $data);
        $this->view('home/profile', $data);
        $this->view('templates/footer', $data);
    }

    public function register()
    {

        if (isset($_POST['username'])) {

            $data = [
                'username' => $_POST['username'],
                'email' => $_POST['email'],
                'first_name' => $_POST['first-name'],
                'last_name' => $_POST['last-name'],
                'password' => password_hash($_POST['password'], PASSWORD_BCRYPT),
                // 'verify_password' => $_POST['verify-password'],
            ];
    
            if ($this->model('User_model')->addUser($data)) {
                Flasher::setFlash("register sukses", "coba login", "success");
                header('Location: ' . BASE_URL . '/home/signin');
                exit;
            }
    
            
        }

        return $this->view('signup');

    }
    public function signup() {
        $data['judul'] = 'register';
        $this->view('login_register/signup', $data);
    }
    public function signin() {
        $data['judul'] = 'login';
        $this->view('login_register/signin', $data);
    }

}
