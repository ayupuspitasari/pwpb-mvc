<?php 

class App {
    // comtroller dan method default
    protected $controller = "Home",
                $method = "index",
                $params = [];

    public function __construct()
    {
        $url = $this->parseURL();
        
        //comtroller
        if(isset($url[0])){
            if(file_exists('../app/controllers/' . $url[0] . '.php')){
                $this->controller = $url[0];
                unset($url[0]); // supaya bisa mengambil parameternya nanti. Oleh karena itu, index ke 0 dihapus.
            }
        }
    
        require_once "../app/controllers/" . $this->controller . '.php';
        $this->controller = new $this->controller;

        //method
        if(isset($url[1])) {
            if(method_exists($this->controller, $url[1])){
                $this->method = $url[1];
                unset($url[1]);
            }
        }

        // params (kelola parameternya)
        if(!empty($url)) {
            $this->params = array_values($url);
        }

       // jalankan controller & method, serta kirimkan params jika ada
       call_user_func_array([$this->controller, $this->method], $this->params);

    }

    public function parseURL(){
        if(isset($_GET['url']))
        {
            $url = rtrim($_GET['url'], '/'); //untuk menghapus tanda / diakhir
            $url = filter_var($url, FILTER_SANITIZE_URL); // membersihkan url nya dri karakter" yg tidak sesuai dg kaidah url
            $url = explode('/', $url);
            return $url;
        }   
        else 
        {
            return [$this->controller];
        }
    }   
}