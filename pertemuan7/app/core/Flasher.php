<?php
class Flasher{
    public static function setFlash($pesan,$aksi,$tipe) {
        if (!session_id()) session_start();

        $_SESSION['flash'] = [
            'pesan' => $pesan,
            'aksi' => $aksi,
            'tipe' => $tipe,
        ];
        
    }
    public static function flash() {
        if (!session_id()) session_start();

        if (isset($_SESSION['flash'])) {
            echo '<div class="alert alert-' . $_SESSION ["flash"]["tipe"] . '">
                    <strong>'.$_SESSION["flash"]["pesan"] . '</strong>'. $_SESSION
                    ["flash"]["aksi"].'
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>';

                unset($_SESSION['flash']);          
        }
    }
}


?>